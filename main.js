const images = document.querySelectorAll(".image-to-show");
const timer = document.querySelector(".timer");
const imagesArray = [...images];
const start = document.querySelector(".start");
const stop = document.querySelector(".stop")

let index = 0;
let timeAdding = setInterval( showIMG, 120);

function showIMG(){
    if (index===imagesArray.length){
        index=0;
        showIMG()
    }

if(timer.innerText === "0"){
    imagesArray[index].style.display="block"
}
if(timer.innerText==="3.0"){
    imagesArray[index].classList.add("mirror")
}
if(timer.innerText==="3.00"){
    imagesArray[index].classList.remove("mirror")
    imagesArray[index].style.display ="none"
    timer.innerText=0;
    index++;
    showIMG();
}
timer.innerText=(+timer.innerText +0.1).toFixed(2)
}

stop.addEventListener("click", () => {
    clearInterval(timeAdding);
  });
  
  start.addEventListener("click", () => {
    clearInterval(timeAdding);
    timeAdding = setInterval(showIMG, 100);
  });
  